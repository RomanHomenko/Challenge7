//
//  ViewController.swift
//  Challenge7
//
//  Created by Роман Хоменко on 09.07.2022.
//

import UIKit
//import CoreGraphics

class ViewController: UIViewController {
    var imageView: UIImageView?
    var makeMemeButton: UIButton?
    
    // to save image without text
    var clearImage: UIImage?
    
    // top and bottom alert adder counter
    var counter = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavigationContrroller()
        addViewItems()
    }
}

// MARK: NavigationController setup
extension ViewController {
    func setupNavigationContrroller() {
        title = "Meme Maker"
        addNavigationItems()
    }
    
    func addNavigationItems() {
        let rightBarButton = UIBarButtonItem(image: Image.photoLibrary,
                                             style: .done,
                                             target: self,
                                             action: #selector(takePictureFromLibrary))
        let leftBarButton = UIBarButtonItem(barButtonSystemItem: .reply,
                                            target: self,
                                            action: #selector(shareMemeImage))
        navigationItem.rightBarButtonItem = rightBarButton
        navigationItem.leftBarButtonItem = leftBarButton
    }
}

// MARK: Create new Views with constraints
extension ViewController {
    func createImageView() {
        imageView = UIImageView()
        
        imageView!.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func createButton() {
        makeMemeButton = UIButton()
        makeMemeButton?.backgroundColor = .blue
        makeMemeButton?.setTitle("Make Meme",
                                 for: .normal)
        
        makeMemeButton?.addTarget(self,
                                  action: #selector(addFunnyText),
                                  for: .touchUpInside)
        
        makeMemeButton?.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func addConstraintsToViews() {
        view.addSubview(imageView!)
        view.addSubview(makeMemeButton!)
        
        NSLayoutConstraint.activate([
            imageView!.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            imageView!.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            imageView!.widthAnchor.constraint(equalToConstant: 400),
            imageView!.heightAnchor.constraint(equalToConstant: 600)
        ])
        
        NSLayoutConstraint.activate([
            makeMemeButton!.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            makeMemeButton!.topAnchor.constraint(equalTo: imageView!.bottomAnchor, constant: 20),
            makeMemeButton!.widthAnchor.constraint(equalToConstant: 400),
            makeMemeButton!.heightAnchor.constraint(equalToConstant: 100)
        ])
    }
}

// MARK: Add new Views
extension ViewController {
    func addViewItems() {
        createImageView()
        createButton()
        
        addConstraintsToViews()
    }
}

// MARK: Selector @objc methods
extension ViewController {
    // Take picture from library by UIImagePicker
    @objc func takePictureFromLibrary() {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        
        present(imagePicker, animated: true)
    }
    
    // Create alert with two lines of meme text
    @objc func addFunnyText() {
        guard imageView?.image != nil else { return }
        
        if counter == 0 {
            let alert = UIAlertController(title: "Create meme",
                                          message: "Add some top text",
                                          preferredStyle: .alert)
            alert.addTextField()
            
            let done: UIAlertAction = UIAlertAction(title: "OK",
                                     style: .default,
                                     handler: { _ in
                let render = UIGraphicsImageRenderer(size: self.imageView!.frame.size)
                
                let image = render.image { ctx in
                    //  draw main image from clear
                    let image = self.clearImage
                    
                    image?.draw(at: CGPoint(x: 0,
                                            y: 0))
                    image?.draw(in: self.imageView!.bounds)
                    
                    // draw text on top of image
                    let paragraphStyle = NSMutableParagraphStyle()
                    paragraphStyle.alignment = .center
                    
                    let attrs: [NSAttributedString.Key: Any] = [
                        .font: Text.memeText,
                        .paragraphStyle: paragraphStyle
                    ]
                    
                    let string = alert.textFields?.first?.text
                    
                    let attributedString = NSAttributedString(string: string!,
                                                              attributes: attrs)
                    attributedString.draw(with: CGRect(x: 0,
                                                       y: 10,
                                                       width: self.imageView!.frame.width,
                                                       height: Text.memeText.pointSize + 10),
                                          options: .usesLineFragmentOrigin,
                                          context: nil)
                    
                }
                
                self.counter += 1
                
                self.imageView?.image = image
                self.addFunnyText()
            })
            
            alert.addAction(done)
            
            present(alert, animated: true)
        } else {
            let alert = UIAlertController(title: "Create meme",
                                          message: "Add some bottom text",
                                          preferredStyle: .alert)
            alert.addTextField()
            
            let done: UIAlertAction = UIAlertAction(title: "OK",
                                     style: .default,
                                     handler: { _ in
                let render = UIGraphicsImageRenderer(size: self.imageView!.frame.size)
                
                let image = render.image { ctx in
                    //  draw main image
                    let image = self.imageView?.image
                    
                    image?.draw(at: CGPoint(x: 0,
                                            y: 0))
                    image?.draw(in: self.imageView!.bounds)
                    
                    // draw text on bottom of image
                    let paragraphStyle = NSMutableParagraphStyle()
                    paragraphStyle.alignment = .center
                    
                    let attrs: [NSAttributedString.Key: Any] = [
                        .font: Text.memeText,
                        .paragraphStyle: paragraphStyle
                    ]
                    
                    let string = alert.textFields?.first?.text
                    
                    let attributedString = NSAttributedString(string: string!,
                                                              attributes: attrs)
                    attributedString.draw(with: CGRect(x: 0,
                                                       y: self.imageView!.frame.height - 60,
                                                       width: self.imageView!.frame.width,
                                                       height: Text.memeText.pointSize + 10),
                                          options: .usesLineFragmentOrigin,
                                          context: nil)
                    
                }
                
                self.imageView?.image = image
            })
            
            alert.addAction(done)
            
            present(alert, animated: true)
            counter = 0
        }
        
    }
    
    @objc func shareMemeImage() {
        guard let image = imageView?.image else { return }
        
        let imageToShare = [image]
        let activityVC = UIActivityViewController(activityItems: imageToShare,
                                                  applicationActivities: nil)
        activityVC.popoverPresentationController?.sourceView = view
        
        present(activityVC, animated: true)
    }
}

// MARK: add delegates to navController and pickerController
extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[.originalImage] as? UIImage,
              let imageView = imageView else { return }
        
        imageView.image = selectedImage
        clearImage = selectedImage
        dismiss(animated: true)
    }
}
